import { XCircleIcon } from '@heroicons/react/24/solid';

export interface AlertInfo {
  autoClose: boolean;
  active: boolean;
  message: string;
  type: string;
}

export interface AlertProps {
  alert: AlertInfo;
  handleClose: () => void;
}

//Componente Alert
const Alert = ({ alert, handleClose }: AlertProps) => {
  if (alert?.autoClose) {
    setTimeout(() => {
      handleClose();
    }, 9000);
  }

  let color = alert.type === 'success' ? 'green-100' : 'red-500';
  color = alert.type == '' ? 'indigo-100' : color;

  return (
    <>
      {alert?.active && (
        <div x-data="true" className={`bg-${color} p-5 w-full rounded mb-8 z-100`}>
          <div className="flex space-x-3">
            <div className="flex-1 leading-tight text-sm text-black font-medium">{alert.message}</div>
            <button type="button">
              <XCircleIcon className="w-6 h-6 text-gray-600" onClick={handleClose} />
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default Alert;
