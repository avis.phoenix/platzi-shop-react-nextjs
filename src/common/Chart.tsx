import { Chart as ChartJS, CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend, ChartData } from 'chart.js';
import { Bar } from 'react-chartjs-2';

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend);

export interface CategoryData {
  [key: string]: number;
}

interface ChartProps {
  chartData: ChartData<'bar', CategoryData>;
  titleName: string;
}

export default function Chart({ chartData, titleName }: ChartProps) {
  return (
    <>
      <Bar
        data={chartData}
        options={{
          plugins: {
            title: {
              display: true,
              text: titleName,
              font: {
                size: 20,
              },
            },
            legend: {
              display: false,
              position: 'right',
            },
          },
        }}
      />
    </>
  );
}
