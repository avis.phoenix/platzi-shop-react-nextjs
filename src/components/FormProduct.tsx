import { Dispatch, SetStateAction, useRef, useState } from 'react';
import { addProduct, updateProduct } from '@services/api/products';
import { CheckBadgeIcon, XCircleIcon } from '@heroicons/react/24/solid';
import { AlertInfo } from '@common/Alert';
import { APIProductData } from '@models/Products';
import useFetch from '@hooks/useFetch';
import endPoints from '@services/api';
import { useRouter } from 'next/router';

enum ResponseType {
  success,
  error,
  none,
}

interface FormProductProps {
  setOpen?: Dispatch<SetStateAction<boolean>>;
  setAlert?: Dispatch<SetStateAction<AlertInfo>>;
  product?: APIProductData;
}

export default function FormProduct({ setOpen, setAlert, product }: FormProductProps) {
  const formRef = useRef<HTMLFormElement>(null);
  const [errImg, setErrImg] = useState<string>('');
  const [response, setResponse] = useState<ResponseType>(ResponseType.none);
  const categories = useFetch<Array<string>>(endPoints.categories.getCategoriesList);
  const router = useRouter();

  function checkData(data: APIProductData) {
    // las REGEX se aplican sobre Strings (toString())
    setErrImg('');
    let pass = true;
    if (!data.images[0].match(/^.+\.(jpg|jpeg|png)$/g)) {
      //verifica la extensión
      setErrImg('Invalid file extension');
      pass = false;
    }

    return pass;
  }

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setResponse(ResponseType.none);
    if (formRef?.current) {
      //formData captura cada elemento del input
      const formData = new FormData(formRef.current);
      //Destructurar los datos
      const data: APIProductData = {
        title: formData.get('title')?.toString() ?? '',
        price: parseInt(formData.get('price')?.toString() ?? '0'),
        description: formData.get('description')?.toString() ?? '',
        category: formData.get('category')?.toString() ?? '',
        images: [(formData.get('images') as File)?.name ?? ''],
        discountPercentage: 0,
        rating: 0,
        stock: 0,
        brand: formData.get('brand')?.toString() ?? '',
        thumbnail: (formData.get('images') as File)?.name ?? '',
      };
      if (checkData(data) && setAlert && setOpen) {
        if (product && product.id) {
          updateProduct(product.id, data)
            .then(() => {
              router.push('/dashboard/products/');
            })
            .catch((reason) => {
              setResponse(ResponseType.error);
              console.log(reason);
              setAlert({
                active: true,
                message: `Error: ${reason.message.toString()}`,
                type: 'error',
                autoClose: false,
              });
            });
        } else {
          addProduct(data)
            .then((response) => {
              setResponse(ResponseType.success);
              console.log(response);
              /** formData.set('title', "");
            formData.set('price', "");
            formData.set('description', "");
            formData.set('category', "");
            formData.set('images', ""); **/
              setAlert({
                active: true,
                message: 'Product added succesfully',
                type: 'success',
                autoClose: false,
              });
              setOpen(false);
            })
            .catch((reason) => {
              setResponse(ResponseType.error);
              console.log(reason);
              setAlert({
                active: true,
                message: `Error: ${reason.message.toString()}`,
                type: 'error',
                autoClose: false,
              });
            });
        }
      }
    }
  };

  if (formRef?.current && product) {
    //formData captura cada elemento del input
    const formData = new FormData(formRef.current);
    formData.set('category', product?.category);
  }

  return (
    <form ref={formRef} onSubmit={handleSubmit}>
      <div className="overflow-hidden">
        <div className="px-4 py-5 bg-white sm:p-6">
          <div className="grid grid-cols-6 gap-6">
            <div className="col-span-6 sm:col-span-3">
              <label htmlFor="title" className="block text-sm font-medium text-gray-700">
                Title
              </label>
              <input
                type="text"
                name="title"
                id="title"
                required
                minLength={5}
                defaultValue={product?.title}
                className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
              />
            </div>
            <div className="col-span-6 sm:col-span-3">
              <label htmlFor="price" className="block text-sm font-medium text-gray-700">
                Price
              </label>
              <input
                type="number"
                name="price"
                id="price"
                required
                min={0}
                defaultValue={product?.price}
                className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
              />
            </div>
            <div className="col-span-6 sm:col-span-3">
              <label htmlFor="brand" className="block text-sm font-medium text-gray-700">
                Brand
              </label>
              <input
                type="text"
                name="brand"
                id="brand"
                required
                minLength={1}
                defaultValue={product?.brand}
                className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
              />
            </div>
            <div className="col-span-6 sm:col-span-3">
              <label htmlFor="category" className="block text-sm font-medium text-gray-700">
                Category
              </label>
              <select
                id="category"
                name="category"
                autoComplete="category-name"
                defaultValue={product?.category}
                className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              >
                {categories?.map((category) => (
                  <option key={`Cat_${category}`} value={category}>
                    {category}
                  </option>
                ))}
              </select>
            </div>
            <div className="col-span-6">
              <label htmlFor="description" className="block text-sm font-medium text-gray-700">
                Description
              </label>
              <textarea
                name="description"
                id="description"
                autoComplete="description"
                rows={3}
                required
                minLength={5}
                maxLength={255}
                defaultValue={product?.description}
                className="form-textarea mt-1 block w-full mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
              />
            </div>
            <div className="col-span-6">
              <div>
                <label className="block text-sm font-medium text-gray-700">Cover photo</label>
                <div className={`mt-1 flex justify-center px-6 pt-5 pb-6 border-2 ${errImg ? 'border-red-300' : 'border-gray-300'} border-dashed rounded-md`}>
                  <div className="space-y-1 text-center">
                    <svg className="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                      <path
                        d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                        strokeWidth={2}
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      />
                    </svg>
                    <div className="flex text-sm text-gray-600">
                      <label
                        htmlFor="images"
                        className="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
                      >
                        <span>Upload a file</span>
                        <input id="images" name="images" type="file" className="sr-only" />
                      </label>
                      <p className="pl-1">or drag and drop</p>
                    </div>
                    <p className="text-xs text-gray-500">PNG, JPG, GIF up to 10MB</p>
                    <p className="text-red-500 text-xs italic">{}</p>
                  </div>
                </div>
                {errImg && <p className="text-red-500 text-xs italic">{errImg}</p>}
              </div>
            </div>
          </div>
        </div>
        <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
          <button
            type="submit"
            className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            Save
          </button>
        </div>
        <div className="flex justify-center">
          {response == ResponseType.success && <CheckBadgeIcon className="h-6 w-6 text-green-500" />}
          {response == ResponseType.error && <XCircleIcon className="h-6 w-6 text-red-500" />}
        </div>
      </div>
    </form>
  );
}
