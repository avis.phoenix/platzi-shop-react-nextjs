import Modal from '@common/Modal';
import { LockClosedIcon } from '@heroicons/react/24/solid';
import { useAuth } from '@hooks/useAuth';
import { AxiosError } from 'axios';
import { useRouter } from 'next/router';
import { useRef, useState } from 'react';

export default function LoginPage() {
  const emailRef = useRef<HTMLInputElement>(null);
  const passRef = useRef<HTMLInputElement>(null);
  const auth = useAuth();
  const [errorLogin, setErrorLogin] = useState<string | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [open, setOpen] = useState<boolean>(false);
  const route = useRouter();

  const submitHandler = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const email = emailRef.current?.value;
    const password = passRef.current?.value;

    console.log(email, password);
    setErrorLogin(null);
    setLoading(true);

    if (email && password) {
      auth
        ?.signIn(email, password)
        .then(() => {
          route.push('/dashboard');
        })
        .catch((reason: AxiosError) => {
          setLoading(false);
          if (reason.response?.status === 401) {
            setErrorLogin('Wrong E-mail or Password.');
          } else {
            setErrorLogin('Unexpected error, try again.');
            console.error(reason);
          }
          setOpen(true);
        });
    }
  };
  return (
    <>
      <Modal title="Authorization Error" open={open} setOpen={setOpen}>
        <div className="px-8">
          <div className="mt-2">
            <p className="text-sm text-gray-500"> {errorLogin}</p>
          </div>
          <div className="px-4 py-3 flex justify-center">
            <button
              type="button"
              className="inline-flex w-full justify-center rounded-md bg-red-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-red-500 sm:ml-3 sm:w-auto"
              onClick={() => setOpen(false)}
            >
              Got it
            </button>
          </div>
        </div>
      </Modal>
      <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="max-w-md w-full space-y-8">
          <div>
            <img className="mx-auto h-12 w-auto" src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg" alt="Workflow" />
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Sign in to your account</h2>
          </div>
          <form className="mt-8 space-y-6" onSubmit={submitHandler}>
            <input type="hidden" name="remember" defaultValue="true" />
            <div className="rounded-md shadow-sm -space-y-px">
              <div>
                <label htmlFor="email-address" className="sr-only">
                  Email address
                </label>
                <input
                  id="email-address"
                  name="email"
                  type="email"
                  autoComplete="email"
                  required
                  className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Email address"
                  ref={emailRef}
                />
              </div>
              <div>
                <label htmlFor="password" className="sr-only">
                  Password
                </label>
                <input
                  id="password"
                  name="password"
                  type="password"
                  autoComplete="current-password"
                  required
                  className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Password"
                  ref={passRef}
                />
              </div>
            </div>

            <div className="flex items-center justify-between">
              <div className="flex items-center">
                <input id="remember-me" name="remember-me" type="checkbox" className="h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded" />
                <label htmlFor="remember-me" className="ml-2 block text-sm text-gray-900">
                  Remember me
                </label>
              </div>

              <div className="text-sm">
                <a href="#" className="font-medium text-indigo-600 hover:text-indigo-500">
                  Forgot your password?
                </a>
              </div>
            </div>

            <div className="flex flex-col items-center">
              <button
                disabled={loading}
                type="submit"
                className="drop-shadow-md group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                  <LockClosedIcon className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400" aria-hidden="true" />
                </span>
                {loading ? <>Validate user...</> : <>Sign in</>}
              </button>
              {loading && (
                <div className="relative h-4 w-4">
                  <span className="flex absolute h-4 w-4 top-0 right-0 -mt-1 -mr-1">
                    <span className="animate-ping absolute inline-flex h-full w-full rounded-full bg-indigo-300 opacity-75"></span>
                    <span className="relative inline-flex rounded-full h-4 w-4 bg-indigo-400"></span>
                  </span>
                </div>
              )}
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
