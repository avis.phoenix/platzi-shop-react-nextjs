import { AlertInfo } from '@common/Alert';
import { useState } from 'react';

//useAlert permite tener opciones
const useAlert = (options: AlertInfo | null = null) => {
  const defaultOptions: AlertInfo = {
    active: false,
    message: '',
    type: '',
    autoClose: false,
  };
  const [alert, setAlert] = useState<AlertInfo>({
    ...defaultOptions, //con .... se destructura los valores
    ...options,
  });

  const toggleAlert = () => {
    setAlert({
      active: !alert.active,
      message: alert.message,
      type: alert.type,
      autoClose: alert.autoClose,
    });
  };

  const closeAlert = () => {
    if (alert.active === true) {
      setAlert({
        active: false,
        message: alert.message,
        type: alert.type,
        autoClose: alert.autoClose,
      });
    }
  };

  return {
    alert,
    setAlert,
    toggleAlert,
    closeAlert,
  };
};

export default useAlert;
