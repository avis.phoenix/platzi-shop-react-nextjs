import React, { useState, useContext, createContext, ReactNode } from 'react';
import Cookie from 'js-cookie'; //Nos ayuda asignar a nuestro navegador las cookies que esté recibiendo en el momento de la autenticación
import axios from 'axios'; //Para el manejo de las peticiones como GET, PUT, POST, DELETE
import endPoints from '@services/api/';
import { APIAuth } from '@models/Auth';

interface AuthStore {
  user: APIAuth | null;
  signIn: (email: string, password: string) => Promise<void>;
  signOut: () => void;
}

const AuthContext = createContext<AuthStore | undefined>(undefined); //Se crea un nuevo context gracias a la api de react
//Se crea la encapsulación de nuestra aplicación
export function ProviderAuth({ children }: { children: ReactNode }) {
  const auth = useProvideAuth();
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
}
//Permite exponer cierta información que se estará requiriendo
export const useAuth = () => {
  return useContext(AuthContext);
};
//Captar la información del usuario
function useProvideAuth() {
  const [user, setUser] = useState<APIAuth | null>(null);

  const options = {
    headers: {
      accept: '*/*',
      'Content-Type': 'application/json;charset=utf-8',
    },
  };
  const signIn = async (email: string, password: string) => {
    const username = email.split('@')[0];
    const { data } = await axios.post<APIAuth>(endPoints.auth.login, { username, password }, options);
    if (data) {
      Cookie.set('token', data.token);
      axios.defaults.headers.Authorization = `Bearer ${data.token}`;
      setUser(data);
    }
  };
  const signOut = () => {
    Cookie.remove('token');
    setUser(null);
    delete axios.defaults.headers.Authorization;
    window.location.href = '/login';
  };

  return {
    user,
    signIn,
    signOut,
  };
}
