import { useState, useEffect } from 'react'; //Se importa desde react
import axios from 'axios'; //Con axios vamos a realizar las peticiones

export default function useFetch<T>(endpoint: string) {
  const [data, setData] = useState<T | null>(null);

  async function fetchData() {
    const response = await axios.get(endpoint);
    setData(response.data);
  }
  useEffect(() => {
    fetchData().catch((reason) => {
      console.error(reason);
    });
  }, [endpoint]);

  return data;
}
