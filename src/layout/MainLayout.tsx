import { ReactNode } from 'react';
import Header from '@components/Header';
import Nav from '@common/Nav';

export default function MainLayout({ children }: { children: ReactNode }) {
  return (
    <div className="min-h-full">
      <Header />
      <Nav />
      <main className="max-w-7xl mx-auto py-5 sm:px-6 lg:px-8">{children}</main>
    </div>
  );
}
