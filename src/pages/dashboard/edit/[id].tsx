import FormProduct from '@components/FormProduct';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import axios from 'axios';
import endPoints from '@services/api';
import { APIProductData } from '@models/Products';
import Alert, { AlertInfo } from '@common/Alert';

//La función edit retorna el formulario para pasarle un nuevo valor del producto
export default function Edit() {
  const [product, setProduct] = useState<APIProductData | null>(null); //Donde se almacena la información
  const router = useRouter();

  useEffect(() => {
    const { id } = router.query;
    const validId = id ? id.toString() : '0';
    if (!router.isReady) return; //Si no tenemos el id, retorna y no hace el llamado
    //Se hace el llamado con getProduct
    async function getProduct() {
      const response = await axios.get(endPoints.products.getProduct(parseInt(validId)));
      setProduct(response.data);
    }
    getProduct();
  }, [router?.isReady]); //Para saber que ya está disponible

  const defaultError: AlertInfo = {
    active: true,
    autoClose: false,
    message: 'No product selected',
    type: 'error',
  };

  return <>{product ? <FormProduct product={product} /> : <Alert alert={defaultError} handleClose={() => {}}></Alert>}</>;
}
