import Chart, { CategoryData } from '@common/Chart';
import { PencilIcon, TrashIcon } from '@heroicons/react/24/solid';
import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/24/outline';
import useFetch from '@hooks/useFetch';
import endPoints from '@services/api';
import { useState } from 'react';
import { APIProductLisResponse } from '@models/Products';
import { ChartData } from 'chart.js';
import Link from 'next/link';

const headers = ['Name', 'Category', 'Price', 'Id', 'Actions'];

const PRODUCT_LIMIT = 20;

export default function Dashboard() {
  const [offset, setOffset] = useState<number>(0);
  const productList = useFetch<APIProductLisResponse>(endPoints.products.getProducts(PRODUCT_LIMIT, offset));
  const products = productList?.products;

  //Con categoryNames se conoce cuántos elementos están dentro de las categorías
  const categoryNames = products?.map((product) => product.category);

  //Con countOcurrences se conoce el número de ocurrencias (frecuencia) que aparece cada categoría
  //Lógiga constante = (array, value) => array.reduce((a,v) => (v === value? a + 1: a), 0);
  const countOcurrences = (arr: Array<string>) => arr.reduce((prev: CategoryData, curr) => ((prev[curr] = ++prev[curr] || 1), prev), {});
  /* console.log(countOcurrences(categoryCount)) */
  //El objeto data contiene la información necesaria para el gráfico
  const data: ChartData<'bar', CategoryData> = {
    datasets: [
      {
        label: 'Categories',
        data: countOcurrences(categoryNames ?? []),
        borderWidth: 2,
        backgroundColor: ['#ffbb11', '#c0c0c0', '#50AF95', '#f3ba2f', '#2a71d0'],
      },
    ],
  };

  const nextPage = () => {
    const page = offset + PRODUCT_LIMIT;
    let total = productList?.total;
    total = total ? total - 1 : 99;
    if (page <= total) {
      setOffset(page);
    }
  };
  const prevPage = () => {
    const page = offset - PRODUCT_LIMIT;
    if (offset != page) {
      setOffset(page < 0 ? 0 : page);
    }
  };

  const actions = (productId: number) => {
    return (
      <>
        <Link title="Edit" href={`/dashboard/edit/${productId}`}>
          <PencilIcon className="h-6 w-6 text-indigo-600 hover:text-indigo-900" />
        </Link>
        <button className="ml-2" title="Delete">
          <TrashIcon className="h-6 w-6 text-red-600 hover:text-red-900 " />
        </button>
      </>
    );
  };

  return (
    <>
      <div className="mb-8 mt-2">
        <Chart chartData={data} titleName="Categories" />
      </div>
      <div className="flex flex-1 justify-between mt-10">
        <button
          onClick={prevPage}
          disabled={offset == 0}
          className="relative inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50"
        >
          <ChevronLeftIcon className="h-5 w-5" aria-hidden="true" />
        </button>
        <p className="text-sm font-medium text-gray-900">Page {offset / PRODUCT_LIMIT + 1}</p>
        <button onClick={nextPage} className="relative ml-3 inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50">
          <ChevronRightIcon className="h-5 w-5" aria-hidden="true" />
        </button>
      </div>
      <div className="flex flex-col">
        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    {headers.map((header) => (
                      <th key={`K-${header}`} scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        {header}
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {products?.map((product) => (
                    <tr key={`PI-${product.id}`}>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div className="flex items-center">
                          <div className="flex-shrink-0 h-10 w-10">
                            <img className="h-10 w-10 rounded-full" src={product.images[0]} alt="" />
                          </div>
                          <div className="ml-4">
                            <div className="text-sm font-medium text-gray-900">{product.title}</div>
                          </div>
                        </div>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div className="text-sm text-gray-900">{product.category}</div>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">${product.price}</span>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{product.id}</td>
                      <td className="px-6 py-4 flex">{actions(product.id ? product.id : 0)}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
