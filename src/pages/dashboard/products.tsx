import { PencilIcon, PlusCircleIcon, TrashIcon } from '@heroicons/react/24/solid';
import { useEffect, useState } from 'react';
import FormProduct from '@components/FormProduct';
import Modal from '@common/Modal';
import axios from 'axios';
import endPoints from '@services/api';
import useAlert from '@hooks/useAlert';
import Alert from '@common/Alert';
import { APIProductData, APIProductLisResponse } from '@models/Products';
import { deleteProduct } from '@services/api/products';
import Link from 'next/link';

const headers = ['Name', 'Category', 'Price', 'Id', 'Actions'];

export default function Products() {
  const [open, setOpen] = useState<boolean>(false);
  const [products, setProducts] = useState<Array<APIProductData>>([]);
  const { alert, setAlert, closeAlert } = useAlert();

  //useEffect cuando los componentes hagan render se pueda traer los productos
  useEffect(() => {
    async function getProducts() {
      const response = await axios.get<APIProductLisResponse>(endPoints.products.getAllProducts); //Get all
      setProducts(response.data.products);
    }

    getProducts().catch((error) => {
      console.log(error);
    });
  }, [alert]);

  const handleDelete = (id: number) => {
    deleteProduct(id)
      .then(() => {
        setAlert({
          active: true,
          message: 'Delete product successfuly',
          type: 'success',
          autoClose: true,
        });
      })
      .catch((reason) => {
        console.error(reason);
        setAlert({
          active: true,
          message: 'Cannot delete the product.',
          type: 'error',
          autoClose: false,
        });
      });
  };

  const actions = (productId: number) => {
    return (
      <>
        <Link title="Edit" href={`/dashboard/edit/${productId}`}>
          <PencilIcon className="h-6 w-6 text-indigo-600 hover:text-indigo-900" />
        </Link>
        <button className="ml-2" title="Delete" onClick={() => handleDelete(productId)}>
          <TrashIcon className="h-6 w-6 text-red-600 hover:text-red-900 " />
        </button>
      </>
    );
  };

  return (
    <>
      <Alert alert={alert} handleClose={closeAlert} />
      <div className="lg:flex lg:items-center lg:justify-between">
        <div className="min-w-0 flex-1">
          <h2 className="text-2xl font-bold leading-7 text-gray-900 sm:truncate sm:text-3xl sm:tracking-tight">List of Products</h2>
        </div>
        <div className="mt-5 flex lg:ml-4 lg:mt-0">
          <span className="sm:ml-3">
            <button
              onClick={() => {
                setOpen(true);
              }}
              type="button"
              className="inline-flex items-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            >
              <PlusCircleIcon className="h-6 w-6" />
              Add product
            </button>
          </span>
        </div>
      </div>
      <div className="flex flex-col">
        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    {headers.map((header) => (
                      <th key={`K-${header}`} scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        {header}
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {products?.map((product) => (
                    <tr key={`PI-${product.id}`}>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div className="flex items-center">
                          <div className="flex-shrink-0 h-10 w-10">
                            <img className="h-10 w-10 rounded-full" src={product.images[0]} alt="" />
                          </div>
                          <div className="ml-4">
                            <div className="text-sm font-medium text-gray-900">{product.title}</div>
                          </div>
                        </div>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div className="text-sm text-gray-900">{product.category}</div>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">${product.price}</span>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{product.id}</td>
                      <td className="px-6 py-4 flex">{actions(product.id ?? 0)}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <Modal title={'Add Product'} open={open} setOpen={setOpen}>
        <FormProduct setOpen={setOpen} setAlert={setAlert} />
      </Modal>
    </>
  );
}
