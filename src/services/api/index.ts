const API = process.env.NEXT_PUBLIC_API_URL;

const endPoints = {
  auth: {
    login: `${API}/auth/login`,
  },
  products: {
    getAllProducts: `${API}/products`,
    getProduct: (id: number) => `${API}/products/${id}`,
    getProducts: (limit: number, offset: number) => `${API}/products?limit=${limit}&skip=${offset}`,
    addProducts: `${API}/products/add`,
    updateProducts: (id: number) => `${API}/products/${id}`,
    deleteProducts: (id: number) => `${API}/products/${id}`,
  },
  categories: {
    getCategoriesList: `${API}/products/categories`,
    getCategoryItems: (category: string) => `${API}/categories/${category}`,
  },
};

export default endPoints;
