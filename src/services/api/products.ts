import axios from 'axios';
import endPoints from '@services/api';
import { APIProductData } from '@models/Products';

const addProduct = async (body: APIProductData) => {
  const config = {
    headers: {
      accept: '*/*', //Permite cualquier petición
      'Content-Type': 'application/json',
    },
  };
  const response = await axios.post(endPoints.products.addProducts, body, config);
  return response.data; //data contiene la respuesta delll servidor
};

const deleteProduct = async (id: number) => {
  const response = await axios.delete(endPoints.products.deleteProducts(id));
  return response.data;
};

const updateProduct = async (id: number, body: APIProductData) => {
  const config = {
    headers: {
      accept: '*/*', //Permite cualquier petición
      'Content-Type': 'application/json',
    },
  };
  const response = await axios.put(endPoints.products.updateProducts(id), body, config); //PUT

  return response.data; //data contiene la respuesta del servidor
};

export { addProduct, deleteProduct, updateProduct };
